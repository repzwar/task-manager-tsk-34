package ru.pisarev.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {
    @Nullable List<Session> findAllByUserId(@Nullable String userId);

    void removeByUserId(@Nullable String userId);
}
